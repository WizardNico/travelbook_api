namespace TravelBook.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tab_registrazioni",
                c => new
                    {
                        reg_PK = c.Int(nullable: false, identity: true),
                        reg_estensione_immagine = c.String(maxLength: 7),
                        reg_nota = c.String(),
                        reg_luogo = c.String(nullable: false, maxLength: 50),
                        reg_area = c.String(nullable: false, maxLength: 50),
                        reg_indirizzo = c.String(nullable: false, maxLength: 50),
                        reg_data = c.DateTime(nullable: false),
                        reg_latitudine = c.Double(nullable: false),
                        reg_longitudine = c.Double(nullable: false),
                        reg_via_FK = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.reg_PK)
                .ForeignKey("dbo.tab_viaggi", t => t.reg_via_FK)
                .Index(t => t.reg_via_FK);
            
            CreateTable(
                "dbo.tab_viaggi",
                c => new
                    {
                        via_PK = c.Int(nullable: false, identity: true),
                        via_nome = c.String(maxLength: 20),
                        via_estensione_immagine = c.String(maxLength: 7),
                        via_data_partenza = c.DateTime(nullable: false),
                        via_data_ritorno = c.DateTime(nullable: false),
                        via_completato = c.Boolean(nullable: false),
                        via_attivo = c.Boolean(nullable: false),
                        via_ute_FK = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.via_PK)
                .ForeignKey("dbo.tab_utenti", t => t.via_ute_FK)
                .Index(t => t.via_ute_FK);
            
            CreateTable(
                "dbo.tab_utenti",
                c => new
                    {
                        ute_PK = c.Int(nullable: false, identity: true),
                        ute_email = c.String(nullable: false, maxLength: 80),
                        ute_password = c.String(nullable: false, maxLength: 300),
                        ute_data_registrazione = c.DateTime(nullable: false),
                        ute_nuova_password = c.String(maxLength: 300),
                        ute_codice_conferma = c.String(maxLength: 30),
                        ute_attivo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ute_PK);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.tab_viaggi", "via_ute_FK", "dbo.tab_utenti");
            DropForeignKey("dbo.tab_registrazioni", "reg_via_FK", "dbo.tab_viaggi");
            DropIndex("dbo.tab_viaggi", new[] { "via_ute_FK" });
            DropIndex("dbo.tab_registrazioni", new[] { "reg_via_FK" });
            DropTable("dbo.tab_utenti");
            DropTable("dbo.tab_viaggi");
            DropTable("dbo.tab_registrazioni");
        }
    }
}
