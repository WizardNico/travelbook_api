﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelBook.Entities {
	[Table("tab_viaggi")]
	public partial class Viaggio {
		[Key]
		public int via_PK { get; set; }



		[StringLength(20)]
		public string via_nome { get; set; }

		[StringLength(7)]
		public string via_estensione_immagine { get; set; }

		[Required]
		public DateTime via_data_partenza { get; set; }

		[Required]
		public DateTime via_data_ritorno { get; set; }

		public bool via_completato { get; set; }

		public bool via_attivo { get; set; }



		public virtual ICollection<Registrazione> Registrazioni { get; set; }



		[Required]
		public int via_ute_FK { get; set; }
		public virtual Utente Utente { get; set; }



		public Viaggio() { Registrazioni = new HashSet<Registrazione>(); }
	}
}