﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelBook.Entities {
	[Table("tab_utenti")]
	public partial class Utente {
		[Key]
		public int ute_PK { get; set; }



		[Required]
		[StringLength(80)]
		public string ute_email { get; set; }

		[Required]
		[StringLength(300)]
		public string ute_password { get; set; }

		public DateTime ute_data_registrazione { get; set; }

		[StringLength(300)]
		public string ute_nuova_password { get; set; }

		[StringLength(30)]
		public string ute_codice_conferma { get; set; }

		public bool ute_attivo { get; set; }



		public virtual ICollection<Viaggio> Viaggi { get; set; }



		public Utente() { Viaggi = new HashSet<Viaggio>(); }
	}
}