﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace TravelBook.Entities {
	[Table("tab_registrazioni")]
	public partial class Registrazione {
		[Key]
		public int reg_PK { get; set; }



		[StringLength(7)]
		public string reg_estensione_immagine { get; set; }

		public string reg_nota { get; set; }
		
		[Required]
		[StringLength(50)]
		public string reg_luogo { get; set; }

		[Required]
		[StringLength(50)]
		public string reg_area { get; set; }

		[Required]
		[StringLength(50)]
		public string reg_indirizzo { get; set; }

		[Required]
		public DateTime reg_data { get; set; }

		[Required]
		public Double reg_latitudine { get; set; }

		[Required]
		public Double reg_longitudine { get; set; }



		[Required]
		public int reg_via_FK { get; set; }
		public virtual Viaggio Viaggio { get; set; }



		public Registrazione() { }
	}
}