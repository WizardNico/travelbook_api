using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using TravelBook.Entities;

namespace TravelBook.Entities {
	public partial class DbTravelBook : DbContext {
		public virtual DbSet<Viaggio> Viaggi { get; set; }
		public virtual DbSet<Utente> Utenti { get; set; }
		public virtual DbSet<Registrazione> Registrazioni { get; set; }

		public DbTravelBook()
			: base("name=dbTravelBook") {
		}


		protected override void OnModelCreating(DbModelBuilder modelBuilder) {
			//tab_utente
			modelBuilder.Entity<Utente>()
				.HasMany(e => e.Viaggi)
				.WithRequired(e => e.Utente)
				.HasForeignKey(e => e.via_ute_FK)
				.WillCascadeOnDelete(false);

			//tab_viaggio
			modelBuilder.Entity<Viaggio>()
				.HasMany(e => e.Registrazioni)
				.WithRequired(e => e.Viaggio)
				.HasForeignKey(e => e.reg_via_FK)
				.WillCascadeOnDelete(false);

			//tab_registrazione
			modelBuilder.Entity<Registrazione>();
		}
	}
}
