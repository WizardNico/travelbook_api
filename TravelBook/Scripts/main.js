function frameOpen(id, page, top, left, right, bottom, width, height) {
	var style = 'position:absolute;';
	if (top != null)
		style += 'top:' + top + ';';

	if (left != null)
		style += 'left:' + left + ';';

	if (right != null)
		style += 'right:' + right + ';';

	if (bottom != null)
		style += 'bottom:' + bottom + ';';

	if (width != null)
		style += 'width:' + width + ';';

	if (height != null)
		style += 'height:' + height + ';';

	var frame = '';
	frame += '<div id="f' + id + '" style="' + style + '">';
	frame += '   <iframe id="f' + id + 'frm" src="' + noCacheUrl(page) + '" style="border:none; position:absolute; left:0px; top:0px; width:100%; height:100%;"></iframe>';
	frame += '</div>';

	$('.divMain').append(frame);
}

function setFramePage(id, page) {
	document.getElementById('f' + id + 'frm').src = noCacheUrl(page);
}

function winFrameOpen(id, page, title, width, height, top) {
	//creo la finestra
	var window = '';
	window += '<div id="w' + id + '" style="position:absolute; left:0px; right:0px; bottom:0px; top:0px;">';
	window += '   <input id="w' + id + 'onClose" type="button" style="display:none;" />';
	window += '   <div class="opacity" style="z-index:2;"></div>';
	window += '   <div class="winFrame" style="position:relative; width:' + width + '; height:' + height + '; margin-top:' + top + '; margin-left:auto; margin-right:auto; z-index:2;">';
	window += '      <div id="w' + id + 'header" class="winFrameHeader" style="position:absolute; left:0px; right:0px; top:0px;">';
	window += '         <span class="winFrameTitle">' + title + '</span>';
	window += '         <input type="button" class="btnWinFrameClose" onclick="winFrameClose(\'' + id + '\');" />';
	window += '      </div>';
	window += '      <div id="w' + id + 'body" style="position:absolute; left:0px; right:0px; bottom:0px;">';
	window += '         <iframe id="w' + id + 'frm" src="' + noCacheUrl(page) + '" style="border:0px; width:100%; height:100%; position:absolute; left:0px; top:0px;"></iframe>';
	window += '      </div>';
	window += '   </div>';
	window += '</div>';

	//inserisco la finestra nella pagina
	$('.divMain').append(window);

	//lascio il dovuto spazio all'header della finestra
	document.getElementById('w' + id + 'body').style.top = document.getElementById('w' + id + 'header').clientHeight + 'px';
}

function winFrameClose(id) {
	document.getElementById('w' + id + 'onClose').click();

	$('#w' + id).remove();
}

function getFrameElement(frameId, elementId) {
	if (document.getElementById('f' + frameId + 'frm') == null)
		return (null);

	return (document.getElementById('f' + frameId + 'frm').contentWindow.document.getElementById(elementId));
}

function getWinFrameElement(winFrameId, elementId) {
	if (document.getElementById('w' + winFrameId + 'frm') == null)
		return (null);

	return (document.getElementById('w' + winFrameId + 'frm').contentWindow.document.getElementById(elementId));
}

function noCacheUrl(url) {
	if (url == null || url.length == 0)
		return (null);

	var result = url.indexOf('?') > 0 ? (url + '&') : (url + '?');
	result += 'NoCache=' + (new Date()).getDate().toString() + parseInt((new Date()).getHours() / 4.0).toString();

	return (result);
}

function setWinFrameCloseAction(id, action) {
	document.getElementById('w' + id + 'onClose').onclick = action;
}