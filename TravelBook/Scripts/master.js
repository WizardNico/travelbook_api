document.write('<script type="text/javascript" src="../Scripts/jquery-2.1.3.min.js"></script>');
document.write('<script type="text/javascript" src="../Scripts/jquery.i18n.properties-min-1.0.9.js"></script>');

var timeout = null;
var waitingCount = 0;

window.onerror = function (message, url, line) {
	alert(message + '\n' + (url.split('/')[url.split('/').length - 1]).split('?')[0] + ' (' + line + ')');
}

function loadLanguage(isoCode) {
	jQuery.i18n.properties({
		name: 'lan',
		path: '../Languages/',
		language: isoCode,
		mode: 'both'
	});
}

function apiRequest(partialUrl, method, command, asynchronous, waiting, render) {
	//se richiesto attivo il box di attesa
	if (waiting == true) {
		waitingCount++;

		if (waitingCount == 1) {
			var waitingBox = '';
			waitingBox += '<div id="waitingBox" class="waitingBox">';
			waitingBox += '   <img src="..\\App_Theme\\images\\ATTESA.gif" alt="" />';
			waitingBox += '</div>';

			$('.divMain').append(waitingBox);
		}
	}

	//trasformo il command in un set di parametri (in formato standard) da inserire nella richiesta ajax
	var bodyParams = null;
	var uriParams = '';
	if (method.toUpperCase() == 'POST' || method.toUpperCase() == 'PUT')
		bodyParams = JSON.stringify(command);

	if (method.toUpperCase() == 'GET' || method.toUpperCase() == 'DELETE')
		uriParams = jQuery.param(command);

	//richiesta ajax
	$.ajax({
		url: '../api/' + partialUrl + (uriParams.length > 0 ? ('?' + uriParams) : ''),
		data: bodyParams,
		datatype: 'json',
		method: method,
		async: asynchronous,
		contentType: 'application/json'
	})
	//se la richiesta � andata a buon fine:
	.done(function (model) {
		render(model);

		//disattivo il box di attesa (solo se era stato attivato)
		if (waiting == true) {
			waitingCount--;

			if (waitingCount == 0)
				$('#waitingBox').remove();
		}
	})
	//se la richiesta non � andata a buon fine:
	.error(function (request, status, error) {
		if (window.top != window.self)
			top.window.location.href = '../Default.html';
		else
			window.location.href = '../Default.html';
	});
}

function apiPost(controllerName, actionName, command, waiting, render) {
	apiRequest(controllerName + '/' + actionName, 'POST', command, true, waiting, render);
}

function apiGet(controllerName, actionName, command, waiting, render) {
	apiRequest(controllerName + '/' + actionName, 'GET', command, true, waiting, render);
}

function apiPut(controllerName, actionName, command, waiting, render) {
	apiRequest(controllerName + '/' + actionName, 'PUT', command, true, waiting, render);
}

function apiDelete(controllerName, actionName, command, waiting, render) {
	apiRequest(controllerName + '/' + actionName, 'DELETE', command, true, waiting, render);
}

function jsonDate(htmlDate) {
	if (htmlDate == null || htmlDate.lenght == 0)
		return (null);

	var splittedDate = htmlDate.split('/');
	var dayStr = splittedDate[0];
	var monthStr = splittedDate[1];
	var yearStr = splittedDate[2];

	var formattedDateStr = yearStr + '-' + monthStr + '-' + dayStr;

	return (formattedDateStr);
}

function htmlDate(jsonDate) {
	if (jsonDate == null || jsonDate.lenght == 0)
		return (null);

	var splittedDate = jsonDate.split('T');
	splittedDate = splittedDate[0].split('-');
	var yearStr = splittedDate[0];
	var monthStr = splittedDate[1];
	var dayStr = splittedDate[2];

	var formattedDateStr = dayStr + '/' + monthStr + '/' + yearStr;

	return (formattedDateStr);
}

function htmlDateTime(jsonDate) {
	if (jsonDate == null || jsonDate.lenght == 0)
		return (null);

	var splittedDate = jsonDate.split('T');
	splittedDate = splittedDate[0].split('-');
	var yearStr = splittedDate[0];
	var monthStr = splittedDate[1];
	var dayStr = splittedDate[2];

	var splittedTime = jsonDate.split('T');
	splittedTime = splittedTime[1].split(':');
	hoursStr = splittedTime[0];
	minutesStr = splittedTime[1];

	var formattedDateStr = dayStr + '/' + monthStr + '/' + yearStr + ' ' + hoursStr + ':' + minutesStr;

	return (formattedDateStr);
}

function controllerTranslationError(controllerName, errorCode, errorDescription) {
	if (window[controllerName.toLowerCase() + '_cterr_' + errorCode] === undefined)
		return (errorDescription);

	return (window[controllerName.toLowerCase() + '_cterr_' + errorCode]);
}

function textPreview(text, maxLength) {
	if (text == null)
		return ('');

	//se la stringa � troppo lunga la interrompo
	var preview;
	var textLength = text.length;
	if (textLength > maxLength) {
		preview = text.substring(0, maxLength - 3);
		preview += '...';
	}
	else
		preview = text;

	//per questioni grafiche il testo deve essere in caratteri minuscoli
	preview = preview.toLowerCase();

	return (preview);
}

function getPageParam(paramName) {
	return (decodeURIComponent((new RegExp('[?|&]' + paramName + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null);
}

function onScrollBottom(divId, render) {
	var scrollHeight = document.getElementById(divId).scrollHeight;
	var offsetHeight = document.getElementById(divId).offsetHeight;
	var scrollTop = document.getElementById(divId).scrollTop;

	if (((scrollHeight - offsetHeight) - scrollTop) <= 0)
		render();
}

function delay(seconds, funct) {
	var milliseconds = seconds * 1000;

	if (timeout != null)
		clearTimeout(timeout);

	timeout = setTimeout(function () {
		funct();

		timeout = null;
	}, milliseconds);
}

function syncApiGet(controllerName, actionName, command) {
	var result = null;

	apiRequest(controllerName + '/' + actionName, 'GET', command, false, false, function (model) {
		result = model;
	});

	return (result);
}

function syncApiPost(controllerName, actionName, command) {
	var result = null;

	apiRequest(controllerName + '/' + actionName, 'POST', command, false, false, function (model) {
		result = model;
	});

	return (result);
}

function syncApiPut(controllerName, actionName, command) {
	var result = null;

	apiRequest(controllerName + '/' + actionName, 'PUT', command, false, false, function (model) {
		result = model;
	});

	return (result);
}

function syncApiDelete(controllerName, actionName, command) {
	var result = null;

	apiRequest(controllerName + '/' + actionName, 'DELETE', command, false, false, function (model) {
		result = model;
	});

	return (result);
}

function getFieldVal(fieldId) {
	if ($('#' + fieldId).val() == null || $('#' + fieldId).val().length == 0)
		return (null);
	else
		return ($('#' + fieldId).val());
}

function getCheckVal(checkId) {
	return (document.getElementById(checkId).checked);
}

function scrollToBottom(scrollId) {
	var scroll = document.getElementById(scrollId);
	scroll.scrollTop = scroll.scrollHeight;
}

function htmlTime(jsonDateTime) {
	if (jsonDateTime == null || jsonDateTime.lenght == 0)
		return (null);

	var timeStr = jsonDateTime;
	if (timeStr.indexOf('T') > 0)
		timeStr = timeStr.split('T')[1];

	timeStr = timeStr.substring(0, 5);

	return (timeStr);
}

function formValidate(formId) {
	//prelevo tutti i campi di input della form considerata
	var inputs = $('form[id="' + formId + '"] input, form[id="' + formId + '"] textarea, form[id="' + formId + '"] select');

	//validazione
	var valid = true;
	for (var i = 0; i < inputs.length; i++) {
		var errorMessage = null;
		var regexpr = null;

		//controllo del formato di un indirizzo e-mail
		regexpr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
		if (inputs[i].getAttribute('data-type') != null && inputs[i].getAttribute('data-type') == 'email')
			if (inputs[i].value != null && inputs[i].value.length > 0 && regexpr.test(inputs[i].value) == false)
				errorMessage = _validator_email;

		//controllo del formato di una data
		regexpr = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/;
		if (inputs[i].getAttribute('data-type') != null && inputs[i].getAttribute('data-type') == 'date')
			if (inputs[i].value != null && inputs[i].value.length > 0 && regexpr.test(inputs[i].value) == false)
				errorMessage = _validator_data;

		//controllo del formato di un numero
		regexpr = /^[\d\.]+$/;
		if (inputs[i].getAttribute('data-type') != null && inputs[i].getAttribute('data-type') == 'number')
			if (inputs[i].value != null && inputs[i].value.length > 0 && regexpr.test(inputs[i].value) == false)
				errorMessage = _validator_numero;

		//controllo della lunghezza minima del campo
		if (inputs[i].getAttribute('data-min') != null && inputs[i].value.length < parseInt(inputs[i].getAttribute('data-min')))
			errorMessage = _validator_obbligatorio_minimo + ' ' + parseInt(inputs[i].getAttribute('data-min')) + ' ' + _validator_obbligatorio_caratteri;

		//controllo di compilazione del campo obbligatorio
		if (inputs[i].getAttribute('data-min') != null && inputs[i].value.length < parseInt(inputs[i].getAttribute('data-min')) && parseInt(inputs[i].getAttribute('data-min')) == 1)
			errorMessage = _validator_obbligatorio;

		//controllo della lunghezza massima del campo
		if (inputs[i].getAttribute('data-max') != null && inputs[i].value.length > parseInt(inputs[i].getAttribute('data-max')))
			errorMessage = _validator_obbligatorio_massimo + ' ' + parseInt(inputs[i].getAttribute('data-max')) + ' ' + _validator_obbligatorio_caratteri;

		//stampa del messaggio di errore
		if (errorMessage != null && document.getElementById(inputs[i].id + '_err') != null) {
			$('#' + inputs[i].id + '_err').html(errorMessage);
			$('#' + inputs[i].id + '_err').show();
		}

		//pulizia del messaggio di errore
		if (errorMessage == null && document.getElementById(inputs[i].id + '_err') != null) {
			$('#' + inputs[i].id + '_err').hide();
			$('#' + inputs[i].id + '_err').empty();
		}

		//naturalmente basta un solo errore per ottenere un risultato negativo
		if (errorMessage != null)
			valid = false;
	}

	//risultato
	return (valid);
}

function getBrowserLanguage() {
	var iso = (navigator.language || navigator.userLanguage);
	iso = iso.toLocaleLowerCase();
	iso = iso.split('-')[0];

	return (iso);
}

function setCookie(name, value) {
	var expirationDate = new Date();
	expirationDate.setTime(expirationDate.getTime() + (30 * 24 * 60 * 60 * 1000));
	document.cookie = name + '=' + encodeURIComponent(value) + '; expires=' + expirationDate.toUTCString();
}

function getCookie(name) {
	name += '=';
	var cookies = document.cookie.split(';');
	for (var i = 0; i < cookies.length; i++) {
		var cookie = cookies[i];
		while (cookie.charAt(0) == ' ')
			cookie = cookie.substring(1);

		if (cookie.indexOf(name) != -1)
			return (decodeURIComponent(cookie.substring(name.length, cookie.length)));
	}

	return (null);
}

function deleteCookies() {
	var cookies = document.cookie.split(';');
	for (var i = 0; i < cookies.length; i++) {
		var cookie = cookies[i];
		var pos = cookie.indexOf('=');
		var name = pos > -1 ? cookie.substr(0, pos) : cookie;
		document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
	}
}

function apiDocument(controllerName, actionName, command) {
	window.location.href = '../api/' + controllerName + '/' + actionName + '?' + jQuery.param(command);
}

function apiPush(controllerName, actionName, maxDelaySeconds, render) {
	setTimeout(function () {
		var cmd = new Object();

		apiGet(controllerName, actionName, cmd, false, function (model) {
			if (model.Success == true)
				render(model.Data);

			apiPush(controllerName, actionName, maxDelaySeconds, render);
		});
	}, (maxDelaySeconds * 1000));
}