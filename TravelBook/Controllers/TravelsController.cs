﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using TravelBook.Utility;
using TravelBook.Errors;
using TravelBook.Commands.Travel;
using TravelBook.Entities;
using TravelBook.Models.Travel;
using System.IO;
using System.Drawing;
using System.Threading.Tasks;
using System.Globalization;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace TravelBook.Controllers {
	public class TravelsController : ApiController {
		private const string TRAVELS_IMG_PATH = "images/travels/";
		private const string TAGS_IMG_PATH = "images/tags/";
		private const int MAX_PAGE_SIZE = 300;

		private DbTravelBook dbTravelBook;
		public TravelsController() { dbTravelBook = new DbTravelBook(); }

		[Authorize]
		[ActionName("addTravel")]
		public Model<Int64?> POST([FromBody] AddTravelCMD cmd) {
			//controllo il titolo
			if (String.IsNullOrEmpty(cmd.Title) == true)
				return (new Model<Int64?>(TravelError.EmptyTitle));

			//controllo data e ora
			if (cmd.DepartureDate == null)
				return (new Model<Int64?>(TravelError.EmptyDepartureDate));

			//controllo data e ora
			if (cmd.ArrivalDate == null)
				return (new Model<Int64?>(TravelError.EmptyArrivalDate));

			//Controllo che la data di partenza non sia maggiore o uguale di quella di arrivo
			if (cmd.DepartureDate.Date >= cmd.ArrivalDate.Date)
				return (new Model<Int64?>(TravelError.InvalidDate));

			//Controlle che l'intervallo inserito non sia a ridosso di un'altro viaggio
			foreach (Viaggio v in dbTravelBook.Viaggi) {
				for (DateTime date = v.via_data_partenza.Date; date.Date < v.via_data_ritorno.Date; date = date.AddDays(1)) {
					if (date == cmd.DepartureDate)
						return (new Model<Int64?>(TravelError.AlreadyUsedDate));
				}
			}

			//inserisco nel dababase il nuovo avvistamento
			Viaggio travel = new Viaggio();
			travel.via_nome = cmd.Title;
			travel.via_data_partenza = cmd.DepartureDate;
			travel.via_data_ritorno = cmd.ArrivalDate;
			travel.via_ute_FK = (int)HttpContext.Current.Session["userId"];
			if (cmd.Activate == true)
				travel.via_attivo = true;
			else
				travel.via_attivo = false;
			travel.via_completato = false;
			dbTravelBook.Viaggi.Add(travel);

			dbTravelBook.SaveChanges();
			return (new Model<Int64?>(travel.via_PK));
		}

		[Authorize]
		[ActionName("removeTravel")]
		public Model<Object> DELETE([FromUri] RemoveTravelCMD cmd) {
			//controllo l'identificatore
			if (cmd.TravelID == null)
				return (new Model<Object>(Errors.TravelError.EmptyTravelId));

			Viaggio travel = dbTravelBook.Viaggi
				.Where(via => via.via_PK == cmd.TravelID)
				.FirstOrDefault();

			if (travel == null)
				return (new Model<Object>(Errors.TravelError.TravelNotFound));

			//eliminazione
			List<Registrazione> tags = travel.Registrazioni
				.ToList();

			//Cancello l'immagine relativa al viaggio se è stata inserita
			if (!String.IsNullOrEmpty(travel.via_estensione_immagine))
				File.Delete(HttpContext.Current.Server.MapPath("~/Public/" + TRAVELS_IMG_PATH + travel.via_PK + "." + travel.via_estensione_immagine));

			foreach (Registrazione tag in tags) {
				//Cancello l'immagine relativa al tag se è stata inserita
				if (!String.IsNullOrEmpty(tag.reg_estensione_immagine))
					File.Delete(HttpContext.Current.Server.MapPath("~/Public/" + TAGS_IMG_PATH + tag.reg_PK + "." + tag.reg_estensione_immagine));
			}

			dbTravelBook.Registrazioni.RemoveRange(tags);
			dbTravelBook.Viaggi.Remove(travel);

			dbTravelBook.SaveChanges();
			return (new Model<Object>());
		}

		[Authorize]
		[ActionName("getTravel")]
		public Model<Travel> GET([FromUri] GetTravelCMD cmd) {
			//deve essere specificato l'identificatore del viaggio
			if (cmd.TravelID == null)
				return (new Model<Travel>(Errors.TravelError.EmptyTravelId));

			//identificatore dell'utente corrente
			int currentUserId = (int)HttpContext.Current.Session["userId"];

			//prelevo il viaggiodal database
			Travel result = dbTravelBook.Viaggi
				.Where(via => via.via_PK == cmd.TravelID)
				.Select(via => new Travel() {
					Place = via.via_nome,
					DepartureDate = via.via_data_partenza,
					ArrivalDate = via.via_data_ritorno,
					TravelID = via.via_PK,
					ImageUrl = via.via_estensione_immagine,
				})
				.FirstOrDefault();

			//verifico la correttezza dell'identificatore del viaggio
			if (result == null)
				return (new Model<Travel>(Errors.TravelError.TravelNotFound));

			//creo l'URL dell'immagine
			if (result.ImageUrl != null)
				result.ImageUrl = DataManagement.publicUrl(TRAVELS_IMG_PATH + cmd.TravelID + "." + result.ImageUrl);

			return (new Model<Travel>(result));
		}

		[Authorize]
		[ActionName("getTravels")]
		public Model<Travels> GET([FromUri] GetTravelsCMD cmd) {
			Travels result = new Travels();

			//controllo i dati di paginazione
			if (cmd.PageIndex == null)
				return (new Model<Travels>(TravelError.EmptyPageIndex));

			if (cmd.PageSize == null)
				return (new Model<Travels>(TravelError.EmptyPageSize));

			if (cmd.PageIndex < 1)
				return (new Model<Travels>(TravelError.InvalidPageIndex));

			if (cmd.PageSize < 1 || cmd.PageSize > MAX_PAGE_SIZE)
				return (new Model<Travels>(TravelError.InvalidPageSize));

			//inizializzazione della query
			int currentUserId = (int)HttpContext.Current.Session["userId"];

			IQueryable<Viaggio> query = dbTravelBook.Viaggi
				.Where(via => via.via_ute_FK == currentUserId);

			//filtro di ricerca
			if (String.IsNullOrEmpty(cmd.GenericFilter) == false)
				query = query
					.Where(via => via.via_nome.ToUpper().Contains(cmd.GenericFilter.Trim().ToUpper()) == true);

			//ordinamento
			query = query.OrderBy(via => via.via_data_partenza.Year).ThenBy(via => via.via_data_partenza.Month).ThenBy(via => via.via_data_partenza.Day);

			//numero di record trovati
			result.RecordCount = query
				.Count();

			//numero di pagine
			result.PageCount = Pagination.pageCount(result.RecordCount.Value, cmd.PageSize.Value);

			//esecuzione paginata della query e mapping
			result.List = query
				.Skip((int)Pagination.skip(cmd.PageIndex.Value, cmd.PageSize.Value))
				.Take(cmd.PageSize.Value)
				.Select(via => new Travel() {
					TravelID = via.via_PK,
					ImageUrl = via.via_estensione_immagine,
					Place = via.via_nome,
					DepartureDate = via.via_data_partenza,
					ArrivalDate = via.via_data_ritorno,
				})
				.ToList();

			//sistemazione degli URL delle immagini
			foreach (Travel travel in result.List)
				if (travel.ImageUrl != null)
					travel.ImageUrl = DataManagement.publicUrl(TRAVELS_IMG_PATH + travel.TravelID + "." + travel.ImageUrl);

			return (new Model<Travels>(result));
		}

		[Authorize]
		[ActionName("getNextTravel")]
		public Model<Travel> GET([FromUri] GetNextTravelCMD cmd) {
			Travels travelsList = new Travels();

			//inizializzazione della query
			int currentUserId = (int)HttpContext.Current.Session["userId"];

			IQueryable<Viaggio> query = dbTravelBook.Viaggi
				.Where((via => via.via_ute_FK == currentUserId && via.via_completato == false));

			//ordinamento
			query = query.OrderBy(via => via.via_data_partenza.Year).ThenBy(via => via.via_data_partenza.Month).ThenBy(via => via.via_data_partenza.Day);

			//esecuzione paginata della query e mapping
			travelsList.List = query
				.Select(via => new Travel() {
					TravelID = via.via_PK,
					ImageUrl = via.via_estensione_immagine,
					Place = via.via_nome,
					DepartureDate = via.via_data_partenza,
					ArrivalDate = via.via_data_ritorno,
				})
				.ToList();

			Travel travel = new Travel();
			travel = travelsList.List.First();

			if (travel.DepartureDate == DateTime.ParseExact(cmd.DepartureDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture)) {
				//aggiorno lo stato del viaggio
				Viaggio result = dbTravelBook.Viaggi
					.Where(via => via.via_PK == travel.TravelID)
					.FirstOrDefault();
				result.via_attivo = true;
				dbTravelBook.SaveChanges();
				return (new Model<Travel>(travel));
			} else {
				return (new Model<Travel>(TravelError.TravelNotFound));
			}
		}

		[Authorize]
		[ActionName("getActivateTravel")]
		public Model<Travel> GET([FromUri] GetActivateTravelCMD cmd) {
			Travels travelsList = new Travels();

			//inizializzazione della query
			int currentUserId = (int)HttpContext.Current.Session["userId"];

			IQueryable<Viaggio> query = dbTravelBook.Viaggi
				.Where((via => via.via_ute_FK == currentUserId && via.via_attivo == true));

			//ordinamento
			query = query.OrderBy(via => via.via_data_partenza.Year).ThenBy(via => via.via_data_partenza.Month).ThenBy(via => via.via_data_partenza.Day);

			//numero di record trovati
			travelsList.RecordCount = query
				.Count();

			if (travelsList.RecordCount == 0) {
				return (new Model<Travel>(TravelError.TravelNotFound));
			} else {
				//esecuzione paginata della query e mapping
				travelsList.List = query
					.Select(via => new Travel() {
						TravelID = via.via_PK,
						ImageUrl = via.via_estensione_immagine,
						Place = via.via_nome,
						DepartureDate = via.via_data_partenza,
						ArrivalDate = via.via_data_ritorno,
					})
					.ToList();

				return (new Model<Travel>(travelsList.List.First()));
			}
		}

		[Authorize]
		[ActionName("checkTravelOver")]
		public Model<Object> POST([FromBody] CheckTravelOverCMD cmd) {
			Travels travelsList = new Travels();

			//inizializzazione della query
			int currentUserId = (int)HttpContext.Current.Session["userId"];

			IQueryable<Viaggio> query = dbTravelBook.Viaggi
				.Where((via => via.via_ute_FK == currentUserId && via.via_attivo == true && via.via_completato == false));

			//ordinamento
			query = query.OrderBy(via => via.via_data_ritorno.Year).ThenBy(via => via.via_data_ritorno.Month).ThenBy(via => via.via_data_ritorno.Day);

			//esecuzione della query e mapping
			travelsList.List = query
				.Select(via => new Travel() {
					TravelID = via.via_PK,
					ImageUrl = via.via_estensione_immagine,
					Place = via.via_nome,
					DepartureDate = via.via_data_partenza,
					ArrivalDate = via.via_data_ritorno,
				})
				.ToList();


			foreach (Travel travel in travelsList.List) {
				int result = DateTime.Compare(travel.ArrivalDate, DateTime.ParseExact(cmd.TodayDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
				if (result == 0 || result < 0) {
					//aggiorno lo stato del viaggio
					Viaggio viaggio = dbTravelBook.Viaggi
						.Where(via => via.via_PK == travel.TravelID)
						.FirstOrDefault();
					viaggio.via_attivo = false;
					viaggio.via_completato = true;
					dbTravelBook.SaveChanges();
					return (new Model<Object>());
				}
			}
			return (new Model<Object>(TravelError.TravelNotFound));
		}

		[Authorize]
		[ActionName("addPicture")]
		public Model<Object> POST([FromBody] AddPictureCMD cmd) {
			//controllo l'identificatore
			if (String.IsNullOrEmpty(cmd.TravelID) == true)
				return (new Model<Object>(Errors.TravelError.EmptyTravelId));

			//controllo l'estensione
			if (String.IsNullOrEmpty(cmd.Extension) == true)
				return (new Model<Object>(Errors.TravelError.InvalidExtension));

			//controllo l'immagine
			if (String.IsNullOrEmpty(cmd.Image) == true)
				return (new Model<Object>(Errors.TravelError.EmptyImage));

			//Recupero il path
			string path = HttpContext.Current.Server.MapPath("~/Public/" + TRAVELS_IMG_PATH);

			SaveImage(path, cmd.Image.ToString(), cmd.TravelID + "." + cmd.Extension);

			//Recupero l'id relativo al viaggio
			int id = Convert.ToInt32(cmd.TravelID);

			//aggiorno lo stato del viaggio
			Viaggio result = dbTravelBook.Viaggi
				.Where(via => via.via_PK == id)
				.FirstOrDefault();
			result.via_estensione_immagine = cmd.Extension;
			dbTravelBook.SaveChanges();

			return (new Model<Object>());
		}

		private void SaveImage(string fullOutputPath, string encodedImage, string fileName) {
			using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(encodedImage))) {
				using (Bitmap bm2 = new Bitmap(ms)) {
					if (File.Exists(HttpContext.Current.Server.MapPath("~/Public/" + TRAVELS_IMG_PATH + fileName)))
						File.Delete(HttpContext.Current.Server.MapPath("~/Public/" + TRAVELS_IMG_PATH + fileName));

					bm2.Save(fullOutputPath + fileName);
				}
			}
		}

		protected override void Dispose(bool disposing) {
			base.Dispose(disposing);
			dbTravelBook.Dispose();
		}
	}
}