﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Security;
using System.Web.Http;
using System.Net.Http;
using TravelBook.Utility;
using TravelBook.Errors;
using TravelBook.Commands.User;
using TravelBook.Entities;
using TravelBook.Models;

namespace TravelBook.Controllers {
	public class UsersController : ApiController {

		private const int NEW_PASSWORD_LENGTH = 10;
		private const int CONFIRMATION_CODE_LENGTH = 30;
		private const int PASSWORD_MIN_LENGTH = 5;

		private DbTravelBook dbTravelBook;
		public UsersController() { dbTravelBook = new DbTravelBook(); }

		[ActionName("addUser")]
		public Model<Object> POST([FromBody] AddUserCMD cmd) {

			List<Utente> usersList = dbTravelBook.Utenti.ToList();

			//Controllo che non sia nulla la mail, e che sia data nel giusto formato
			if (cmd.Email == null || cmd.Email.ToString().Trim().Length == 0 || !DataManagement.isEmailAddress(cmd.Email))
				return (new Model<Object>(UserErrors.InvalidEmail));

			//Controllo che nella lista di utenti precedentemente creata non ci sia nessun'altro utente con la stessa mail
			foreach (Utente usr in usersList)
				if (usr.ute_email.ToLower().Equals(cmd.Email.ToLower()))
					return (new Model<Object>(UserErrors.AlreadyUsedEmail));

			//controllo che la password sia almeno composta da 5 caratteri
			if (cmd.Password.Length < PASSWORD_MIN_LENGTH)
				return (new Model<Object>(UserErrors.InvalidPassword));

			//Creo l'utente, lo inserisco nel DB e salvo
			Utente newUser = new Utente();
			newUser.ute_email = cmd.Email;
			newUser.ute_password = Security.hashing(cmd.Password);
			newUser.ute_data_registrazione = DateTime.UtcNow;
			newUser.ute_nuova_password = null;
			newUser.ute_attivo = true;
			newUser.ute_codice_conferma = Security.randomString(CONFIRMATION_CODE_LENGTH).ToUpper();
			dbTravelBook.Utenti.Add(newUser);
			dbTravelBook.SaveChanges();

			//spedisco all'utente una e-mail che consentirà di confermare il suo account
			Mail.smtp(Project.EmailSenderHost, Project.EmailSenderAddress, Project.EmailPassword, Project.EmailSenderName, cmd.Email, "Conferma Account", "Conferma il tuo Account al seguente Indirizzo: " + HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/Views/ConfermaUtente.html?Email=" + newUser.ute_email + "&ConfirmationCode=" + newUser.ute_codice_conferma + ".");

			return (new Model<object>());
		}

		[Authorize]
		[ActionName("getUser")]
		public Model<User> GET([FromUri] GetUserCMD cmd) {
			Utente ute = (from u in dbTravelBook.Utenti
							  where u.ute_email.ToString().Equals(cmd.Email.ToString())
							  select u).FirstOrDefault();

			if (ute == null) {
				return (new Model<User>(UserErrors.UserNotFound));
			} else {
				User user = new User();
				user.UserID = ute.ute_PK;
				user.Email = ute.ute_email;
				user.New_Password = ute.ute_nuova_password;
				user.Registration_Date = ute.ute_data_registrazione;

				return (new Model<User>(user));
			}
		}

		[ActionName("newRandomPassword")]
		public Model<Object> PUT([FromBody] NewRandomPasswordCMD cmd) {
			Utente user = dbTravelBook.Utenti
				.Where(ute => ute.ute_email.CompareTo(cmd.Email) == 0)
				.FirstOrDefault();

			//controllo che l'indirizzo e-mail fornito in input sia corretto
			if (user == null)
				return (new Model<Object>(UserErrors.InvalidEmail));

			//creo una nuova password
			string newPassword = Security.randomString(NEW_PASSWORD_LENGTH);
			user.ute_nuova_password = Security.hashing(newPassword);

			//spedisco una e-mail con la nuova password
			Mail.smtp(Project.EmailSenderHost, Project.EmailSenderAddress, Project.EmailPassword, Project.EmailSenderName, cmd.Email, "Nuova Password", "La tua Nuova Password è " + newPassword);

			dbTravelBook.SaveChanges();
			return (new Model<Object>());
		}

		[ActionName("confirmUser")]
		public Model<Object> POST([FromBody] ConfirmUserCMD cmd) {
			Utente user = dbTravelBook.Utenti
				.Where(ute => ute.ute_email.CompareTo(cmd.Email) == 0)
				.FirstOrDefault();

			//controllo l'e-mail
			if (user == null)
				return (new Model<Object>(UserErrors.InvalidEmail));

			//controllo il codice di conferma
			if (user.ute_codice_conferma != null && cmd.ConfirmationCode != null && user.ute_codice_conferma.CompareTo(cmd.ConfirmationCode.ToUpper()) == 0) {
				//confermo l'utente
				user.ute_codice_conferma = null;

				//abilito l'utente ad accedere alle risorse protette
				FormsAuthentication.SetAuthCookie(user.ute_PK.ToString(), false);

				//salvo in sessione l'id dell'utente
				HttpContext.Current.Session["userId"] = user.ute_PK;

				dbTravelBook.SaveChanges();
				return (new Model<Object>());
			} else
				return (new Model<Object>(UserErrors.UserNotFound));
		}

		protected override void Dispose(bool disposing) {
			base.Dispose(disposing);
			dbTravelBook.Dispose();
		}
	}
}