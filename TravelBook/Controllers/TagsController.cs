﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using TravelBook.Utility;
using TravelBook.Errors;
using TravelBook.Commands.Tag;
using TravelBook.Entities;
using TravelBook.Models.Tag;
using System.Data.Entity.Validation;
using System.Globalization;
using TravelBook.Commands.Tags;
using System.IO;
using System.Drawing;

namespace TravelBook.Controllers {
	public class TagsController : ApiController {
		private const string TAGS_IMG_PATH = "images/tags/";
		private const int MAX_PAGE_SIZE = 300;

		private DbTravelBook dbTravelBook;
		public TagsController() { dbTravelBook = new DbTravelBook(); }

		[Authorize]
		[ActionName("addTag")]
		public Model<Int64?> POST([FromBody] AddTagCMD cmd) {
			//controllo il titolo
			if (String.IsNullOrEmpty(cmd.Name) == true)
				return (new Model<Int64?>(TagError.EmptyName));

			//Controllo che l'id del viaggio non sia nullo
			if (String.IsNullOrEmpty(cmd.TravelID) == true)
				return (new Model<Int64?>(TagError.InvalidTravelID));

			//Controllo che la data si settata
			if (String.IsNullOrEmpty(cmd.Date) == true)
				return (new Model<Int64?>(TagError.InvalidDateTime));

			//controllo le coordinate
			if (cmd.Latitude == null)
				return (new Model<Int64?>(TagError.EmptyLatitude));

			if (cmd.Longitude == null)
				return (new Model<Int64?>(TagError.EmptyLongitude));

			//inserisco nel dababase la nuova registrazione
			Registrazione tag = new Registrazione();
			tag.reg_area = cmd.Area;
			tag.reg_luogo = cmd.Name;
			tag.reg_indirizzo = cmd.Address;
			tag.reg_latitudine = cmd.Latitude.Value;
			tag.reg_longitudine = cmd.Longitude.Value;
			tag.reg_data = DateTime.ParseExact(cmd.Date.ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
			tag.reg_via_FK = Convert.ToInt32(cmd.TravelID);

			Tags result = new Tags();
			var travelId = Convert.ToInt32(cmd.TravelID);

			IQueryable<Registrazione> query = dbTravelBook.Registrazioni
				.Where(reg => reg.reg_via_FK == travelId);

			//esecuzione paginata della query e mapping
			result.List = query
				.Select(reg => new Tag() {
					TagID = reg.reg_PK,
					ImageUrl = reg.reg_estensione_immagine,
					Note = reg.reg_nota,
					TravelID = reg.reg_via_FK,
					Date = reg.reg_data,
					Name = reg.reg_luogo,
					Address = reg.reg_indirizzo,
					Area = reg.reg_area,
					Latitude = reg.reg_latitudine,
					Longitude = reg.reg_longitudine,
				})
				.ToList();

			//Controllo che non sia già stato registrato in questo posto
			foreach (Tag reg in result.List)
				if ((reg.Latitude == cmd.Latitude && reg.Longitude == cmd.Longitude) || (cmd.Address.Equals(reg.Address)))
					return (new Model<Int64?>(TagError.AlreadyExistTag));

			dbTravelBook.Registrazioni.Add(tag);
			dbTravelBook.SaveChanges();
			return (new Model<Int64?>(tag.reg_PK));
		}

		[Authorize]
		[ActionName("getCoordinates")]
		public Model<Tags> GET([FromUri] GetCoordinatesCMD cmd) {
			Tags result = new Tags();

			if (cmd.TravelID == null)
				return (new Model<Tags>(TagError.InvalidTravelID));

			IQueryable<Registrazione> query = dbTravelBook.Registrazioni
				.Where(reg => reg.reg_via_FK == cmd.TravelID);

			//numero di record trovati
			result.RecordCount = query
				.Count();

			//esecuzione paginata della query e mapping
			result.List = query
				.Select(reg => new Tag() {
					TagID = reg.reg_PK,
					ImageUrl = reg.reg_estensione_immagine,
					Note = reg.reg_nota,
					TravelID = reg.reg_via_FK,
					Date = reg.reg_data,
					Name = reg.reg_luogo,
					Address = reg.reg_indirizzo,
					Area = reg.reg_area,
					Latitude = reg.reg_latitudine,
					Longitude = reg.reg_longitudine,
				})
				.ToList();

			//sistemazione degli URL delle immagini
			foreach (Tag tag in result.List)
				if (tag.ImageUrl != null)
					tag.ImageUrl = DataManagement.publicUrl(TAGS_IMG_PATH + tag.TagID + "." + tag.ImageUrl);

			return (new Model<Tags>(result));
		}

		[Authorize]
		[ActionName("getTags")]
		public Model<Tags> GET([FromUri] GetTagsCMD cmd) {
			Tags result = new Tags();

			//controllo i dati di paginazione
			if (cmd.PageIndex == null)
				return (new Model<Tags>(TagError.EmptyPageIndex));

			if (cmd.PageSize == null)
				return (new Model<Tags>(TagError.EmptyPageSize));

			if (cmd.TravelID == null)
				return (new Model<Tags>(TagError.InvalidTravelID));

			if (cmd.PageIndex < 1)
				return (new Model<Tags>(TagError.InvalidPageIndex));

			if (cmd.PageSize < 1 || cmd.PageSize > MAX_PAGE_SIZE)
				return (new Model<Tags>(TagError.InvalidPageSize));

			IQueryable<Registrazione> query = dbTravelBook.Registrazioni
				.Where(reg => reg.reg_via_FK == cmd.TravelID);

			//ordinamento
			query = query.OrderByDescending(reg => reg.reg_data.Year).ThenBy(reg => reg.reg_data.Month).ThenBy(reg => reg.reg_data.Day).ThenBy(reg => reg.reg_data.Hour).ThenBy(reg => reg.reg_data.Minute).ThenBy(reg => reg.reg_data.Second);

			//numero di record trovati
			result.RecordCount = query
				.Count();

			//numero di pagine
			result.PageCount = Pagination.pageCount(result.RecordCount.Value, cmd.PageSize.Value);

			//esecuzione paginata della query e mapping
			result.List = query
				.Skip((int)Pagination.skip(cmd.PageIndex.Value, cmd.PageSize.Value))
				.Take(cmd.PageSize.Value)
				.Select(reg => new Tag() {
					TagID = reg.reg_PK,
					ImageUrl = reg.reg_estensione_immagine,
					Note = reg.reg_nota,
					TravelID = reg.reg_via_FK,
					Date = reg.reg_data,
					Name = reg.reg_luogo,
					Address = reg.reg_indirizzo,
					Area = reg.reg_area,
					Latitude = reg.reg_latitudine,
					Longitude = reg.reg_longitudine,
				})
				.ToList();

			//sistemazione degli URL delle immagini
			foreach (Tag tag in result.List)
				if (tag.ImageUrl != null)
					tag.ImageUrl = DataManagement.publicUrl(TAGS_IMG_PATH + tag.TagID + "." + tag.ImageUrl);

			return (new Model<Tags>(result));
		}

		[Authorize]
		[ActionName("getTag")]
		public Model<Tag> GET([FromUri] GetTagCMD cmd) {
			//deve essere specificato l'identificatore del tag
			if (cmd.TagID == null)
				return (new Model<Tag>(TagError.EmptyTagId));

			//identificatore dell'utente corrente
			int currentUserId = (int)HttpContext.Current.Session["userId"];

			//prelevo il tag dal database
			Tag result = dbTravelBook.Registrazioni
				.Where(reg => reg.reg_PK == cmd.TagID)
				.Select(reg => new Tag() {
					Date = reg.reg_data,
					Area = reg.reg_area,
					Latitude = reg.reg_latitudine,
					Longitude = reg.reg_longitudine,
					Name = reg.reg_luogo,
					Note = reg.reg_nota,
					Address = reg.reg_indirizzo,
					ImageUrl = reg.reg_estensione_immagine,
				})
				.FirstOrDefault();

			//verifico la correttezza dell'identificatore del viaggio
			if (result == null)
				return (new Model<Tag>(TagError.TagNotFound));

			//creo l'URL dell'immagine
			if (result.ImageUrl != null)
				result.ImageUrl = DataManagement.publicUrl(TAGS_IMG_PATH + cmd.TagID + "." + result.ImageUrl);

			return (new Model<Tag>(result));
		}

		[Authorize]
		[ActionName("editTag")]
		public Model<Object> POST([FromBody] EditTagCMD cmd) {
			//deve essere specificato l'identificatore del tag
			if (cmd.TagID == null)
				return (new Model<Object>(TagError.EmptyTagId));

			//Prelevo il tag dal database
			Registrazione tag = dbTravelBook.Registrazioni
				.Where(reg => reg.reg_PK == cmd.TagID)
				.FirstOrDefault();

			//Controllo che il tag ottenuto non sia null
			if (tag == null)
				return (new Model<Object>(TagError.TagNotFound));

			//controllo che la nota non sia nula
			if (String.IsNullOrEmpty(cmd.Note) == false)
				tag.reg_nota = cmd.Note;

			//controllo l'estensione che non sia nulla
			if (String.IsNullOrEmpty(cmd.Extension) == false)
				tag.reg_estensione_immagine = cmd.Extension;

			dbTravelBook.SaveChanges();
			return (new Model<Object>());
		}

		[Authorize]
		[ActionName("removeTag")]
		public Model<Object> DELETE([FromUri] RemoveTagCMD cmd) {
			//deve essere specificato l'identificatore del tag
			if (cmd.TagID == null)
				return (new Model<Object>(TagError.EmptyTagId));

			//Prelevo il tag dal database
			Registrazione tag = dbTravelBook.Registrazioni
				.Where(reg => reg.reg_PK == cmd.TagID)
				.FirstOrDefault();

			//Controllo che il tag ottenuto non sia null
			if (tag == null)
				return (new Model<Object>(TagError.TagNotFound));

			//Cancello l'immagine relativa se è stata inserita
			if (!String.IsNullOrEmpty(tag.reg_estensione_immagine))
				File.Delete(HttpContext.Current.Server.MapPath("~/Public/" + TAGS_IMG_PATH + tag.reg_PK + "." + tag.reg_estensione_immagine));

			//eliminazione
			dbTravelBook.Registrazioni.Remove(tag);

			dbTravelBook.SaveChanges();
			return (new Model<Object>());
		}

		[Authorize]
		[ActionName("addPicture")]
		public Model<Object> POST([FromBody] AddPictureCMD cmd) {
			//controllo l'identificatore
			if (String.IsNullOrEmpty(cmd.TagID) == true)
				return (new Model<Object>(Errors.TravelError.EmptyTravelId));

			//controllo l'estensione
			if (String.IsNullOrEmpty(cmd.Extension) == true)
				return (new Model<Object>(Errors.TravelError.InvalidExtension));

			//controllo l'immagine
			if (String.IsNullOrEmpty(cmd.Image) == true)
				return (new Model<Object>(Errors.TravelError.EmptyImage));

			//Recupero il path
			string path = HttpContext.Current.Server.MapPath("~/Public/" + TAGS_IMG_PATH);

			SaveImage(path, cmd.Image.ToString(), cmd.TagID + "." + cmd.Extension);

			//Recupero l'id relativo al viaggio
			int id = Convert.ToInt32(cmd.TagID);

			//aggiorno lo stato del viaggio
			Registrazione result = dbTravelBook.Registrazioni
				.Where(reg => reg.reg_PK == id)
				.FirstOrDefault();
			result.reg_estensione_immagine = cmd.Extension;
			dbTravelBook.SaveChanges();

			return (new Model<Object>());
		}

		private void SaveImage(string fullOutputPath, string encodedImage, string fileName) {
			using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(encodedImage))) {
				using (Bitmap bm2 = new Bitmap(ms)) {
					if (File.Exists(HttpContext.Current.Server.MapPath("~/Public/" + TAGS_IMG_PATH + fileName)))
						File.Delete(HttpContext.Current.Server.MapPath("~/Public/" + TAGS_IMG_PATH + fileName));

					bm2.Save(fullOutputPath + fileName);
				}
			}
		}

		protected override void Dispose(bool disposing) {
			base.Dispose(disposing);
			dbTravelBook.Dispose();
		}
	}
}