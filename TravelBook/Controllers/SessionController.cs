﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TravelBook.Utility;
using TravelBook.Errors;
using TravelBook.Commands.Session;
using TravelBook.Entities;
using TravelBook.Models;
using System.Data.Entity.SqlServer;
using System.Web;
using System.Web.Security;

namespace TravelBook.Controllers {
	public class SessionController : ApiController {
		private DbTravelBook dbTravelBook;

		public SessionController() { dbTravelBook = new DbTravelBook(); }

		[ActionName("login")]
		public Model<Boolean> GET([FromUri] LoginCMD cmd) {
			cmd.Password = Security.hashing(cmd.Password);
			Utente user = (from u in dbTravelBook.Utenti
								where u.ute_email.ToString().ToLower().Equals(cmd.Email.ToString().ToLower()) &&
										u.ute_password.ToString().Equals(cmd.Password.ToString())
								select u).FirstOrDefault();

			if (user != null) {
				Model<Boolean> model;

				//Controllo se è confermato
				if (user.ute_codice_conferma != null) {
					model = new Model<Boolean>(SessionErrors.AccountNotConfirmed);
					model.Data = false;
					return (model);
				}

				//Controllo se è attivo
				if (user.ute_attivo == false) {
					model = new Model<Boolean>(SessionErrors.AccountNotActive);
					model.Data = false;
					return (model);
				}

				model = new Model<Boolean>();
				model.Data = true;

				//salvo in sessione l'id dell'utente loggato
				HttpContext.Current.Session["userId"] = user.ute_PK;

				//abilito l'utente ad accedere alle risorse protette
				FormsAuthentication.SetAuthCookie(user.ute_PK.ToString(), false);

				return (model);

			} else {
				var model = new Model<Boolean>(SessionErrors.LoginError);
				model.Data = false;
				return (model);
			}
		}

		[ActionName("checkSession")]
		public Model<Boolean?> GET([FromUri] CheckSessionCMD cmd) {
			if (HttpContext.Current.Session["userId"] != null)
				return (new Model<Boolean?>(true));
			else
				return (new Model<Boolean?>(false));
		}


		[Authorize]
		[ActionName("logout")]
		public Model<Boolean?> GET([FromUri] LogoutCMD cmd) {
			FormsAuthentication.SignOut();
			HttpContext.Current.Session.Abandon();

			return (new Model<Boolean?>(true));
		}

		protected override void Dispose(bool disposing) {
			base.Dispose(disposing);
			dbTravelBook.Dispose();
		}
	}
}