﻿using System;
using TravelBook.Utility;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Errors {
	public class SessionErrors {
		public static ControllerError LoginError { get { return (new ControllerError(5, "Invalid Credential")); } }
		public static ControllerError AccountNotConfirmed { get { return (new ControllerError(6, "This Account Isn't Confirmed")); } }
		public static ControllerError AccountNotActive { get { return (new ControllerError(7, "This Account Isn't Active")); } }
	}
}