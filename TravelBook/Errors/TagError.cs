﻿using System;
using TravelBook.Utility;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Errors {
	public class TagError {
		public static ControllerError EmptyPageIndex { get { return (new ControllerError(23, "Empty Page Index.")); } }
		public static ControllerError EmptyPageSize { get { return (new ControllerError(24, "Empty Page Size.")); } }
		public static ControllerError InvalidPageIndex { get { return (new ControllerError(25, "Invalid Page Index.")); } }
		public static ControllerError InvalidPageSize { get { return (new ControllerError(26, "Invalid Page Size.")); } }
		public static ControllerError InvalidTravelID { get { return (new ControllerError(27, "Invalid Travel ID.")); } }
		public static ControllerError EmptyTagId { get { return (new ControllerError(28, "Empty Tag ID.")); } }
		public static ControllerError TagNotFound { get { return (new ControllerError(29, "Tag Not Found")); } }
		public static ControllerError NoPermission { get { return (new ControllerError(30, "No Permission To Delete This Tag.")); } }
		public static ControllerError EmptyName { get { return (new ControllerError(31, "Empty Name For This Tag.")); } }
		public static ControllerError EmptyLatitude { get { return (new ControllerError(32, "Empty Latitude.")); } }
		public static ControllerError EmptyLongitude { get { return (new ControllerError(33, "Empty Longitude.")); } }
		public static ControllerError EmptyDate { get { return (new ControllerError(34, "Empty Date.")); } }
		public static ControllerError AlreadyExistTag { get { return (new ControllerError(35, "You've Already Visited This Place")); } }
		public static ControllerError InvalidDateTime { get { return (new ControllerError(36, "Date/Time For This Tag Is Not Correct")); } }
	}
}