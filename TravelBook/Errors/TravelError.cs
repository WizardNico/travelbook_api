﻿using System;
using TravelBook.Utility;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Errors {
	public class TravelError {
		public static ControllerError EmptyPageIndex { get { return (new ControllerError(8, "Empty Page Index.")); } }
		public static ControllerError EmptyPageSize { get { return (new ControllerError(9, "Empty Page Size.")); } }
		public static ControllerError InvalidPageIndex { get { return (new ControllerError(10, "Invalid Page Index.")); } }
		public static ControllerError InvalidPageSize { get { return (new ControllerError(11, "Invalid Page Size.")); } }
		public static ControllerError InvalidUserID { get { return (new ControllerError(12, "Invalid User ID.")); } }
		public static ControllerError EmptyTitle { get { return (new ControllerError(13, "Invalid Title For This Travel.")); } }
		public static ControllerError EmptyDepartureDate { get { return (new ControllerError(14, "Empty Departure Date.")); } }
		public static ControllerError EmptyArrivalDate { get { return (new ControllerError(15, "Empty Arrival Date.")); } }
		public static ControllerError EmptyTravelId { get { return (new ControllerError(16, "Empty Travel ID.")); } }
		public static ControllerError TravelNotFound { get { return (new ControllerError(17, "Travel Not Found")); } }
		public static ControllerError NoPermission { get { return (new ControllerError(18, "No Permission To Delete This Travel.")); } }
		public static ControllerError AlreadyUsedDate { get { return (new ControllerError(19, "There's Already A Travel For This Date")); } }
		public static ControllerError InvalidDate { get { return (new ControllerError(20, "There's An Error On The Date")); } }
		public static ControllerError EmptyImage { get { return (new ControllerError(21, "There's An Error On The Image")); } }
		public static ControllerError InvalidExtension { get { return (new ControllerError(22, "Invalid Extension")); } }
	}
}