﻿using System;
using TravelBook.Utility;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Errors {
	public class UserErrors {
		public static ControllerError InvalidEmail { get { return (new ControllerError(1, "Invalid Email Address.")); } }
		public static ControllerError AlreadyUsedEmail { get { return (new ControllerError(2, "This Email Is Already Used.")); } }
		public static ControllerError InvalidPassword { get { return (new ControllerError(3, "Password Too Short.")); } }
		public static ControllerError UserNotFound { get { return (new ControllerError(4, "User Not Found.")); } }
	}
}