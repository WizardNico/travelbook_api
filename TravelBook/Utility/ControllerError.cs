﻿namespace TravelBook.Utility {
	public class ControllerError {
		public int? ErrorCode { get; set; }
		public string ErrorDescription { get; set; }

		public ControllerError(int? errorCode, string errorDescription) {
			ErrorCode = errorCode;
			ErrorDescription = errorDescription;
		}
	}
}