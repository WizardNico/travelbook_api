﻿using System;
using System.Threading;

namespace TravelBook.Utility {
	public abstract class Daemon {
		private const int SLEEP_HOURS = 0;
		private const int SLEEP_MINUTES = 0;
		private const int SLEEP_SECONDS = 30;
		private const int MAX_SECONDS_AT_THE_START = 150;

		protected abstract void iteration();

		public Daemon() {
			Thread thread = new Thread(work);
			thread.Start();
		}

		private void work() {
			TimeSpan sleepTime = new TimeSpan(SLEEP_HOURS, SLEEP_MINUTES, SLEEP_SECONDS);

			//faccio partire i thread in maniera sfalsata
			Random rnd = new Random();
			int secondsAtTheStart = rnd.Next(0, MAX_SECONDS_AT_THE_START);
			Thread.Sleep(secondsAtTheStart * 1000);

			while (true) {
				iteration();

				Thread.Sleep(sleepTime);
			}
		}
	}
}