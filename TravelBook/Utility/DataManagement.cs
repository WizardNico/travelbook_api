﻿using System;
using System.Net.Mail;
using System.Web;

namespace TravelBook.Utility {
	public class DataManagement {
		public static int age(DateTime birthDate) {
			DateTime today = DateTime.Today;
			int result = today.Year - birthDate.Year;
			if (birthDate > today.AddYears(-result))
				result--;

			return (result);
		}

		public static string publicUrl(string path) { return (HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/Public/" + path); }

		public static bool isEmailAddress(string text) {
			//controllo la struttura della stringa
			if (occurrences(text, "@") != 1)
				return (false);

			string[] textSplit = text.Split('@');
			string beforeAt = textSplit[0];
			string afterAt = textSplit[1];

			if (beforeAt.Length == 0)
				return (false);

			if (afterAt.Length == 0)
				return (false);

			if (occurrences(afterAt, ".") == 0)
				return (false);

			//faccio un ulteriore controllo
			try {
				new MailAddress(text);

				return (true);
			} catch (Exception) {
				return (false);
			}
		}

		public static bool isPhoneNumber(string text) {
			//consento una occorrenza del carattere '+'
			if (occurrences(text, "+") > 1)
				return (false);

			//controllo se la stringa è convertibile in un numero intero
			long number;
			bool result = Int64.TryParse(text.Replace("+", ""), out number);

			return (result);
		}

		private static int occurrences(string text, string pattern) { return ((text.Length - text.Replace(pattern, "").Length)); }
	}
}