﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace TravelBook.Utility {
	public class Security {
		public static string hashing(string text) {
			//caso in cui la stringa è nulla oppure vuota
			if (text == null || text.Length == 0)
				return ("");

			//hasing a livello di byte
			UnicodeEncoding encoding = new UnicodeEncoding();
			byte[] textBytes = encoding.GetBytes(text);
			SHA512 provider = new SHA512CryptoServiceProvider();
			byte[] hashBytes = provider.ComputeHash(textBytes);

			//trasformo il risultato dell'hashing in una stringa
			string result = "";
			foreach (byte hashByte in hashBytes)
				result += string.Format("{0:x2}", hashByte);

			result = result.ToUpper();

			return (result);
		}

		public static string randomString(int length) {
			Random rnd = new Random();
			string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345678901234567890123456789";

			//compongo la stringa casuale prelevenado dal set di caratteri disponibili
			string result = "";
			for (int i = 0; i < length; i++)
				result += chars[rnd.Next(chars.Length)];

			return (result);
		}
	}
}