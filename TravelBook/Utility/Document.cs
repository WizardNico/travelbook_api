﻿using System.Net;
using System.Net.Http;

namespace TravelBook.Utility {
	public class Document {
		public static HttpResponseMessage HttpError { get { return (new HttpResponseMessage(HttpStatusCode.Forbidden)); } }

		public static HttpResponseMessage HttpSuccess { get { return (new HttpResponseMessage(HttpStatusCode.Accepted)); } }

		public static HttpResponseMessage httpResponse(string fileName, string fileExtension, byte[] bytes) {
			HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
			response.Content = new ByteArrayContent(bytes);
			response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
			response.Content.Headers.ContentDisposition.FileName = fileName + "." + fileExtension;

			return (response);
		}
	}
}