﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Xml;

namespace TravelBook.Utility {
	public class ApiClient {
		private HttpClient httpClient;

		public ApiClient(string apiUrl) {
			httpClient = new HttpClient();
			httpClient.BaseAddress = new Uri(apiUrl + "/");
			httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
		}

		public object get(string partialUrl, object command, Type modelType) {
			//richiesta con passaggio dei parametri nell'URL
			HttpResponseMessage response = httpClient.GetAsync(urlParametersRequest(partialUrl, command)).Result;

			//deserializzazione della risposta
			object model = null;
			if (response.IsSuccessStatusCode == true)
				model = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result, modelType);

			return (model);
		}

		public object post(string partialUrl, object command, Type modelType) {
			//richiesta con passaggio dei parametri nel body
			HttpResponseMessage response = httpClient.PostAsync(partialUrl, new StringContent(JsonConvert.SerializeObject(command), Encoding.UTF8, "application/json")).Result;

			//deserializzazione della risposta
			object model = null;
			if (response.IsSuccessStatusCode == true)
				model = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result, modelType);

			return (model);
		}

		public object put(string partialUrl, object command, Type modelType) {
			//richiesta con passaggio dei parametri nel body
			HttpResponseMessage response = httpClient.PutAsync(partialUrl, new StringContent(JsonConvert.SerializeObject(command), Encoding.UTF8, "application/json")).Result;

			//deserializzazione della risposta
			object model = null;
			if (response.IsSuccessStatusCode == true)
				model = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result, modelType);

			return (model);
		}

		public object delete(string partialUrl, object command, Type modelType) {
			//richiesta con passaggio dei parametri nell'URL
			HttpResponseMessage response = httpClient.DeleteAsync(urlParametersRequest(partialUrl, command)).Result;

			//deserializzazione della risposta
			object model = null;
			if (response.IsSuccessStatusCode == true)
				model = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result, modelType);

			return (model);
		}

		public T get<T>(string partialUrl, object command) { return ((T)get(partialUrl, command, typeof(T))); }

		public T post<T>(string partialUrl, object command) { return ((T)post(partialUrl, command, typeof(T))); }

		public T put<T>(string partialUrl, object command) { return ((T)put(partialUrl, command, typeof(T))); }

		public T delete<T>(string partialUrl, object command) { return ((T)delete(partialUrl, command, typeof(T))); }

		public XmlDocument get(string partialUrl, object command) {
			//richiesta con passaggio dei parametri nell'URL
			HttpResponseMessage response = httpClient.GetAsync(urlParametersRequest(partialUrl, command)).Result;

			//parsing XML
			StreamReader streamReader = new StreamReader(response.Content.ReadAsStreamAsync().Result);
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.LoadXml(streamReader.ReadToEnd());

			return (xmlDocument);
		}

		private string urlParametersRequest(string partialUrl, object command) {
			PropertyInfo[] properties = command.GetType().GetProperties();
			string result = partialUrl + "?";
			foreach (PropertyInfo property in properties)
				if (property.GetValue(command, null) != null)
					if (property.PropertyType == typeof(DateTime?) || property.PropertyType == typeof(DateTime))
						result += property.Name + "=" + ((DateTime)property.GetValue(command, null)).ToString("yyyyMMdd HH\\:mm\\:ss\\.fff") + "&";
					else
						if (property.PropertyType == typeof(TimeSpan?) || property.PropertyType == typeof(TimeSpan))
							result += property.Name + "=" + ((TimeSpan)property.GetValue(command, null)).ToString("hh\\:mm\\:ss\\.fff") + "&";
						else
							result += property.Name + "=" + property.GetValue(command, null).ToString() + "&";

			result = result.Substring(0, result.Length - 1);

			return (result);
		}

		public HttpResponseMessage getDocument(string partialUrl, object command) { return (httpClient.GetAsync(urlParametersRequest(partialUrl, command)).Result); }
	}
}