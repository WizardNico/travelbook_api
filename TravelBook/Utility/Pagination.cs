﻿namespace TravelBook.Utility {
	public class Pagination {
		public static long skip(long pageIndex, int pageSize) { return ((pageIndex - 1) * pageSize); }

		public static long pageCount(long recordCount, int pageSize) {
			//ci deve essere almeno una pagina (anche vuota)
			if (recordCount == 0)
				return (1);

			long res = recordCount / pageSize;
			if ((recordCount % pageSize) > 0)
				res++;

			return (res);
		}
	}
}