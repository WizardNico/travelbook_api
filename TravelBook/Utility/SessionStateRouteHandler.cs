﻿using System.Web;
using System.Web.Routing;

namespace TravelBook.Utility {
	public class SessionStateRouteHandler : IRouteHandler {
		IHttpHandler IRouteHandler.GetHttpHandler(RequestContext requestContext) { return (new SessionableControllerHandler(requestContext.RouteData)); }
	}
}