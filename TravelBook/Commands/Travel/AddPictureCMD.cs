﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace TravelBook.Commands.Travel {
	public class AddPictureCMD {
		public String TravelID { set; get; }
		public String Image { set; get; }
		public String Extension { set; get; }
	}
}