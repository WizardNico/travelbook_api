﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Commands.Travel {
	public class AddTravelCMD {
		public DateTime DepartureDate { get; set; }
		public DateTime ArrivalDate { get; set; }
		public String Title { get; set; }
		public Boolean Activate { get; set; }
	}
}