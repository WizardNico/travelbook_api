﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Commands.Travel {
	public class GetTravelsCMD {
		public String GenericFilter { get; set; }
		public Int64? PageIndex { get; set; }
		public Int32? PageSize { get; set; }
	}
}