﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Commands.Session {
	public class LoginCMD {
		public String Email { get; set; }
		public String Password { get; set; }
	}
}