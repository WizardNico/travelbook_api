﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace TravelBook.Commands.Tags {
	public class AddPictureCMD {
		public String TagID { set; get; }
		public String Image { set; get; }
		public String Extension { set; get; }
	}
}