﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Commands.Tag {
	public class EditTagCMD {
		public Int32? TagID { get; set; }
		public String Note { get; set; }
		public String Extension { get; set; }
	}
}