﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Commands.Tag {
	public class RemoveTagCMD {
		public Int32? TagID { get; set; }
	}
}