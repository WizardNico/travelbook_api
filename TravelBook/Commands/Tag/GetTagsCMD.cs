﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Commands.Tag {
	public class GetTagsCMD {
		public Int64? PageIndex { get; set; }
		public Int32? PageSize { get; set; }
		public Int32? TravelID { get; set; }
	}
}