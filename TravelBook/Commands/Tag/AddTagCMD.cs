﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Commands.Tag {
	public class AddTagCMD {
		public String Name { get; set; }
		public String Date { get; set; }
		public String Area { get; set; }
		public String Address { get; set; }
		public Double? Latitude { get; set; }
		public Double? Longitude { get; set; }
		public String TravelID { get; set; }
	}
}