﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Commands.Tag {
	public class GetTagCMD {
		public Int64? TagID { get; set; }
	}
}