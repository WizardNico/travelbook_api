﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Commands.User {
	public class ConfirmUserCMD {
		public String Email { get; set; }
		public String ConfirmationCode { get; set; }
	}
}