﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Commands.User {
	public class AddUserCMD {
		public String Email { get; set; }
		public String Password { get; set; }
	}
}