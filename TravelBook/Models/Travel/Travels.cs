﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Models.Travel {
	public class Travels {
		public List<Travel> List { get; set; }
		public Int64? RecordCount { get; set; }
		public Int64? PageCount { get; set; }
	}
}