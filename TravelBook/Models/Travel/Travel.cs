﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Models.Travel {
	public class Travel {
		public Int32 TravelID { set; get; }
		public String Place { get; set; }
		public DateTime DepartureDate { get; set; }
		public DateTime ArrivalDate { get; set; }
		public String ImageUrl { get; set; }
	}
}