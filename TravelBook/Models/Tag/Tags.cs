﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Models.Tag {
	public class Tags {
		public List<Tag> List { get; set; }
		public Int64? RecordCount { get; set; }
		public Int64? PageCount { get; set; }
	}
}