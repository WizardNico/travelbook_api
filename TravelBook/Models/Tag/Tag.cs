﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Models.Tag {
	public class Tag {
		public Int32 TagID { set; get; }
		public Int32 TravelID { set; get; }
		public DateTime Date { get; set; }
		public String Name { get; set; }
		public String Note { get; set; }
		public String Area { get; set; }
		public String Address { get; set; }
		public Double Latitude { get; set; }
		public Double Longitude { get; set; }
		public String ImageUrl { get; set; }
	}
}