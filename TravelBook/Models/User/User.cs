﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelBook.Models {
	public class User {
		public Int32 UserID { set; get; }
		public String Email { get; set; }
		public String New_Password { get; set; }
		public DateTime Registration_Date { get; set; }
	}
}