﻿using TravelBook.Utility;
using System;
using System.Globalization;
using System.Reflection;
using System.Threading;
using System.Web.Http;
using System.Web.Routing;

namespace TravelBook {
	public static class WebApiConfig {
		public static void Register(HttpConfiguration config) {
			config.MapHttpAttributeRoutes();

			RouteTable.Routes.MapHttpRoute(
				 name: "DefaultApi",
				 routeTemplate: "api/{controller}/{action}",
				 defaults: new { id = RouteParameter.Optional }
			).RouteHandler = new SessionStateRouteHandler();

			Thread.CurrentThread.CurrentUICulture = new CultureInfo("it-IT");

			//lancio tutti i demoni
			Type[] classes = Assembly.GetExecutingAssembly().GetTypes();
			for (int i = 0; i < classes.Length; i++)
				if (classes[i].Namespace != null && classes[i].Namespace.EndsWith(".Daemons") == true)
					Activator.CreateInstance(classes[i]);
		}
	}
}